<?php
$bloodgroup = trim($_POST['bloodgroup']);
$location = trim($_POST['location']);
$query = "select * from users where bloodgroup='$bloodgroup' and address like '%$location%'";

$db = new DB();
$results = $db->query($query);
$num=  sizeof($results);        
?>

<style>

    tr, th, td {
        background: transparent;
        border: 1px solid #e1e1e1;
        font-style: inherit;
        font-weight: inherit;
        margin: 0;
        padding-left: 7px;
        outline: 0;
        vertical-align: baseline;
    }

    #table-2 {
        border: 1px solid #e3e3e3;
        background-color: #f2f2f2;
        widtd: 100%;
        border-radius: 10px;
        -webkit-border-radius: 10px;
        -moz-border-radius: 10px;
    }

    #table-2 tdead {
        font-family: "Lucida Sans Unicode", "Lucida Grande", sans-serif;
        padding: .2em 0 .2em .5em;
        text-align: left;
        color: #4B4B4B;
        background-color: #C8C8C8;
        background-image: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#e3e3e3), color-stop(.6,#B3B3B3));
        background-image: -moz-linear-gradient(top, #D6D6D6, #B0B0B0, #B3B3B3 90%);
        border-bottom: solid 1px #999;
    }

    tbody {
        display: table-row-group;
        vertical-align: middle;
        border-color: inherit;
    }

    #table-2 td {
        line-height: 20px;
        font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
        font-size: 14px;
        border-bottom: 1px solid #fff;
        border-top: 1px solid #fff;
    }
    
    h3{
        font-family: calibri;
        border-bottom:1px solid  #99b433;
    }

</style>
<div id="content" >
    
 <?php if($num<=0):?>   
  <div class="notices">
    <div class="bg-color-teal">
        <span class="close"></span>
        <div class="notice-icon"><img src="images/shield-user.png"></div>
        <div class="notice-image"><img src="images/armor.png"></div>
        <div class="notice-header fg-color-yellow">Message</div>
        <div class="notice-text">No one found on our database.</div>
    </div>
</div>
<?php endif;?>    
    
    
<?php foreach($results as $row) :?>  
<h3>List of persons matched with your search criteria</h3>    
<table width="100%" id="table-2">
    <tbody>
        <tr class="sectiontableheader">
            <td>Place</td>
            <td>Name</a></td>
            <td>Phone</td>
            <td>Address</td>
            <td>Email</td>
            <td>Blood Group</td>
        </tr>
        <?php
        $count = 0;        //ch
        foreach ($results as $row) {
            $count++;
            echo '<tr class="sectiontableentry1">
		<td style=color:black;font-weight:bold;text-align:center;vertical-align:middle;">' . $count . '</td>
		<td style=color:black;font-weight:bold;text-align:center;vertical-align:middle;">' . $row[1] . '</td>
		<td style=color:black;font-weight:bold;text-align:center;vertical-align:middle;">' . $row[6] . '</td>
                <td style=color:black;font-weight:bold;text-align:center;vertical-align:middle;">' . $row[5] . '</td>
                <td style=color:black;font-weight:bold;text-align:center;vertical-align:middle;">' . $row[3] . '</td>
                <td style=color:black;font-weight:bold;text-align:center;vertical-align:middle;">' . $row[2] . '</td>
	</tr>';
        }
        ?>
    </tbody>
</table>
<?php endforeach;?>    
</div>