<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>NIROB</title>
        <meta name="viewport" content="target-densitydpi=device-dpi, width=device-width, initial-scale=1.0, maximum-scale=1">
        <meta name="description" content="Nirob">
        <meta name="author" content="Ujjal Suttra Dhar">
        <meta name="keywords" content="nirob, ruet, department, css, framework">

        <link href="metroui/css/modern.css" rel="stylesheet">
        <link href="metroui/css/modern-responsive.css" rel="stylesheet">
        <link href="css/nirob.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="css/nivo/default/default.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/nivo/light/light.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/nivo/dark/dark.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/nivo/bar/bar.css" type="text/css" media="screen" />
        <link rel="stylesheet" href="css/nivo/nivo-slider.css" type="text/css" media="screen" />

        
        <script type="text/javascript" src="metroui/js/assets/jquery-1.9.0.min.js"></script>
        <script type="text/javascript" src="js/nirob.js"></script>
        <script type="text/javascript" src="metroui/js/assets/jquery.mousewheel.min.js"></script>
        <script type="text/javascript" src="metroui/js/assets/moment.js"></script>
        <script type="text/javascript" src="metroui/js/assets/moment_langs.js"></script>

        <script type="text/javascript" src="metroui/js/modern/dropdown.js"></script>
        <script type="text/javascript" src="metroui/js/modern/dialog.js"></script>
        <script type="text/javascript" src="metroui/js/modern/accordion.js"></script>
        <script type="text/javascript" src="metroui/js/modern/buttonset.js"></script>
        <script type="text/javascript" src="metroui/js/modern/carousel.js"></script>
        <script type="text/javascript" src="metroui/js/modern/input-control.js"></script>
        <script type="text/javascript" src="metroui/js/modern/pagecontrol.js"></script>
        <script type="text/javascript" src="metroui/js/modern/rating.js"></script>
        <script type="text/javascript" src="metroui/js/modern/slider.js"></script>
        <script type="text/javascript" src="metroui/js/modern/tile-slider.js"></script>
        <script type="text/javascript" src="metroui/js/modern/tile-drag.js"></script>
        <script type="text/javascript" src="metroui/js/modern/calendar.js"></script>

        <title>NIROB</title>

        <style>
            #searchForm input[type=text]{
                min-width:103px !important;
                max-width:148px !important;
                margin-right:0 !important;
                padding:0 !important;
            }
           #searchForm select{
                min-width:60px !important;
                max-width:72px !important;
                margin-right:0 !important;
                padding:0 !important;
                margin-left:47px;
            }

            #searchForm{
                background: rgb(231,242,223);
                width: 251px;
                overflow:hidden;

            }
            #searchForm #title{
                text-align: center;
                font-family: helvetica;
                font-size: 17px;
                margin: 5px;
                background: brown;
                color:white;
                padding: 5px;
            }
            #form{
                margin:5px;

            }

            .element a:link{
                color:black;
                font-weight:bold;
                text-decoration: none;
            }


            .element a:hover{
                color:black;
                font-weight:bold;
                text-decoration: none;
            }


            .element a:visited{
                color:black;
                font-weight:bold;
                text-decoration: none;
            }
        </style>


    </head>
    <body>

        <div id="main">  
            <div class="metrouicss">
                <div class="nav-bar" style="position:fixed;">
                    <div class="nav-bar-inner">

                        <span class="element"><a href="index.php">নিরব</a></span>

                        <span class="divider"></span>

                        <ul class="menu">
                            <li data-role="dropdown">
                                <a href="#">Basic Information</a>
                                <ul class="dropdown-menu">
                                    <li><a href="gon.html">GOn</a></li>

                                    <li><a href="#">SubItem</a></li>
                                </ul>

                            </li>
                            
                     <?php if(isset($_SESSION["logged_in"])):?>
                      
                            <li data-role="dropdown">
                                <a href="#"><?php echo $_SESSION["user_name"];?></a>
                                <ul class="dropdown-menu">
                                    <li><a href="index.php?page=profile">My Profile</a></li>
                                    <li><a href="index.php?page=logout">Logout</a></li>
                                </ul>

                            </li>
                            <li class="divider"></li>
                       <?php endif;?>
                        </ul>
                    </div>
                    
                  
                       
                </div> <!-- nav-->
                        <div class="slider-wrapper theme-default" style="margin-top:55px;">
                            <div id="slider" class="nivoSlider">
                                <img src="images/banner/blood.jpeg" data-thumb="images/banner/blood.jpeg.jpg" alt="" />
                                <img src="images/banner/blood1.jpeg" data-thumb="images/banner/blood1.jpg" alt="" />
                            </div>
                            <div id="htmlcaption" class="nivo-html-caption">
                                <strong>This</strong> is an example of a <em>HTML</em> caption with <a href="#">a link</a>. 
                            </div>
                        </div>

                        <script type="text/javascript" src="js/jquery.nivo.slider.js"></script>
                        <script type="text/javascript">
                            $(window).load(function() {
                                $('#slider').nivoSlider();
                            });
                        </script>
                    