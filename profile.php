
<!--write your html code here -->



<?php if(isset($_GET["msg"])):?>
<div id="content" style="min-height:400px;">
    <div class="notices">
    <div class="bg-color-teal">
        <span class="close"></span>
        <div class="notice-icon"><img src="images/shield-user.png"></div>
        <div class="notice-image"><img src="images/armor.png"></div>
        <div class="notice-header fg-color-yellow">Message</div>
        <div class="notice-text"><?=$_GET["msg"];?></div>
    </div>
</div>
    <?php endif;?>
    
    
    <h3><?php echo "Welcome  <strong>" . $user['fullname'] . '</strong>'; ?></h3>

    <p></p>
    <form method="post" action="index.php?page=update" style="padding:20px;"name="update" onsubmit="return check()">
        <div class="input-control text">
            <input type="text" name="name" placeholder="Enter Full Name" value="<?= $user['fullname']; ?>" />
            <button class="btn-clear"></button>
        </div>
        <div class="input-control text">


            <select name="bloodgroup">
                <option value=""> Select Blood Group</option> 
                <option value="A+" <?php echo ('A+' == $user["bloodgroup"]) ? 'selected' : ''; ?> >A+</option>
                <option value="A-" <?php echo ('A-' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >A-</option>
                <option value="B+" <?php echo ('B+' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >B+</option>
                <option value="B-" <?php echo ('B-' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >B+</option>
                <option value="AB+" <?php echo ('AB+' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >AB+</option>
                <option value="AB-" <?php echo ('AB-' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >AB-</option>
                <option value="O+" <?php echo ('O+' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >O+</option>
                <option value="O-" <?php echo ('O-' == $user["bloodgroup"]) ? 'selected' : ''; ?>
                        >O-</option>
            </select>
        </div>


        <div class="input-control text">
            <input type="email" name="email" placeholder="Enter Email"  value="<?= $user['email']; ?>" disabled/>
            <button class="btn-clear"></button>
        </div>

        <div class="input-control text">
            <input type="text" name="phone" placeholder="Enter your Phone number (use comma for multiple numbers)" value="<?= $user['phone']; ?>" />
            <button class="btn-clear"></button>
        </div>

        <div class="input-control textarea">
            <textarea name="address" placeholder="Enter full address.."  ><?= $user['address']; ?></textarea>
        </div>

        <div class="input-control textarea">
            <textarea name="additional" placeholder="Additional Info or comment"  ><?= $user['additional']; ?></textarea>
        </div>

        <!--<div class="input-control text">
            <input type="text" name="position" placeholder="Current Position"  />
            <button class="btn-clear"></button>
        </div>
        -->

        <div class="input-control password">
            <input type="password" name="newpassword" placeholder="Enter New Password"  />
            <button class="btn-clear"></button>
        </div>
        <div class="input-control password">
            <input type="password" name="newCpassword" placeholder="Confirm New Password"  />
            <button class="btn-clear"></button>
        </div>


        <div class="input-control password">
            <input type="password" name="password" placeholder="Enter Old Password"  required/>
            <button class="btn-clear"></button>
        </div>
        <input type="hidden" name="id" value="<?=$user["id"];?>">
        <input type="submit" value="Update"/>
        <input type="reset"  value="Reset"/>
    </form>


</div>
