            

<footer>
  <div id="footer-content">
   
      
   <div id="urls">
       <h3>নিরব</h3>
       <ul>
           <li>
               <a href="index.php?page=gon">Gon</a>
           </li>
       </ul>
   </div>    
   <div id="sponsors">
       <h3>Sponsors</h3>
       <ul>
           <li>
              <a href="http://www.ruet.ac.bd"> <img src="images/logo_ruet.jpg" alter="RUET" height="60"></a>
           </li>
           <li>
              <a href="https://www.facebook.com/ictfairbd"> <img src="images/ictfair_logo.jpg" height="60"></a>
           </li>
       </ul>
   </div>
      <div id="developers">
          <h3>Developer's Team</h3>
      <ul>
           <li>
               <a href="http://www.ujjalruet.wordpress.com/about">Ujjal Suttra Dhar</a>
           </li>
           <li>
               <a href="http://www.dolar.wordpress.com/about">Mahmudul Hasan</a>
           </li>
       </ul>
   </div>
   <div id="share">
        <a href="https://www.facebook.com/sharer/sharer.php?u=http://invincible.dev&t=Invincible" target="_blank"><img src="images/facebook.png"></a>
        <a href="https://twitter.com/intent/tweet?text=Invincible&amp;url=http://invincible.dev" target="_blank"><img src="images/twitter.png"></a>
        <a href="https://plus.google.com/share?url=http://invincible.dev" target="_blank"><img src="images/googleplus.png">+</a>
    </div>
    
    
    
  </div>
</footer>    
 <div id="copyright">copyright @2013 nirob.org</div>
</div> <!--main --> 
</body>
</html>